﻿
using log4net;
using System.Reflection;
namespace HangFireWebDashboard
{
    public class GlobalUtility
    {

        private static ILog _logger;

        public static ILog Logger
        {
            get
            {
                if (_logger == null)
                {
                    _logger = LogManager.GetLogger(MethodBase.GetCurrentMethod().DeclaringType);
                }
                return _logger;
            }
        }
    }
}
